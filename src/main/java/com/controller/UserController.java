package com.controller;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/health")
    public String getUser() {
        return "User test";
    }

    @GetMapping("/test")
    public String getTest(){
        return "Get test";
    }
}
